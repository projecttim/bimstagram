# Bimstagram
# Je zult een paar programmas moeten downloaden: Xampp en Composer.

1. Open Xampp.
2. Start Apache en MySQL
3. Clone de folder naar je htdocs folder (C:\xampp\htdocs)
4. Check of de naam van de database klopt in de seeder met de database die je hebt op phpmyadmin, als je geen database hebt zul je deze moeten aanmaken.
   Noem de database: "binsta" en maak een user aan met met de naam en wachtwoord: "bit_academy". (of vervang bij elke pdo connectie de naam en wachtwoord met je eigen
   naam en wachtwoord.);
5. "composer i" in de console in je root folder.
6. "php seeder.php" in de console in je root folder.
7. Als alles goed is gegaan krijg je in de console feedback over wat er is toegevoeg aan de database.
7. Open in je browser "localhost/binstagram" (Check of de directory klopt met wat je zelf hebt.)
8. Veel plezier.