<?php

use RedBeanPHP\R;

class BaseController
{
    public function __construct()
    {
        try {
            R::setup('mysql:host=localhost;dbname=binsta', 'bit_academy', 'bit_academy');
        } catch (PDOException $e) {
            echo $e->getmessage();
        }
    }

    public function getBeanById($typeOfBean, $id)
    {
        $validTypes = ['user', 'feed', 'comment'];
        if (in_array($typeOfBean, $validTypes)) {
            return R::load($typeOfBean, $id);
        } else {
            exit(error('404', "Bean $typeOfBean not found"));
        }
    }

    public function showAll($typeOfBean, $limit = null)
    {
        $validTypes = ['user', 'feed', 'comment'];
        if (in_array($typeOfBean, $validTypes)) {
            if ($limit != null) {
                return R::findAll($typeOfBean, " ORDER BY id DESC LIMIT $limit ");
            }
            return R::findAll($typeOfBean);
        } else {
            exit(error('404', "Bean $typeOfBean not found"));
        }
    }

    public function getFullUrl()
    {
        $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' ? 'https' : 'http';
        return $protocol . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    }

    public function authorizeUser($url)
    {
        if ($url != 'user/login' || $url != 'user/register') {
            if ($url == "") {
                header("location: /user/login");
            }
            if (!isset($_SESSION['loggedIn'])) {
                header("location: ../user/login");
                die();
            }
        }
    }
}