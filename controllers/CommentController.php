<?php

use RedBeanPHP\R;

class CommentController extends BaseController
{
    private array $comments = [];
    
    public function __construct()
    {
        $commentsJsonArr = R::findAll('comment');
        foreach ($commentsJsonArr as $commentJsonStr) {
            array_push($this->comments, json_decode($commentJsonStr, true, 512, 0));
        }
    }

    public function index()
    {
        return $this->comments;
    }

    public function newComment($post, $location)
    {
        if (strlen(trim($post['commentContent'])) > 0) {
            $newComment = R::dispense('comment');
            $newComment->comment_content = $post['commentContent'];
            $newComment->feed_id = $post['postId'];
            $newComment->user_id = $_SESSION['loggedIn'];
            R::store($newComment);
            header("location: " . $location);
            unset($_POST);
        }
    }
}