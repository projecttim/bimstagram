<?php

use RedBeanPHP\R;

class FeedController extends BaseController
{
    private array $posts = [];

    public function __construct()
    {
        $feedJsonArr = R::findAll('feed');
        foreach ($feedJsonArr as $feedJsonStr) {
            array_push($this->posts, json_decode($feedJsonStr, true, 512, 0));
        }
    }

    public function index()
    {
        $this->authorizeUser();
        return R::findAll('feed');
    }

    public function clearSes()
    {
        unset($_SESSION['viewType'], $_SESSION['showId'], $_SESSION['viewedUser'], $_SESSION['searchedUsers']);
    }

    public function userFeedView($userId)
    {
        $_SESSION['showId'] = $userId;

        return $this->returnPersonalPosts($userId);
    }

    public function returnSearchResult($query)
    {
        
        $_SESSION['searchedUsers'] = R::find('user', 'username LIKE ? LIMIT 5', ["%$query%"]);
    }

    public function createPost($post)
    {
        $user = R::load('user', $_SESSION['loggedIn']);
        $newPost = R::dispense('feed');
        $newPost->username = $user['username'];
        $newPost->title = $post['postTitle'];
        if (!$post['snippet']) {
            $newPost->snippet = "No code added";
        } else {
            $newPost->snippet = $post['snippet'];
        }
        $newPost->language = $post['language'];
        $newPost->user_id = $_SESSION['loggedIn'];
        $newPost->likes = 0;
        R::store($newPost);
        header("location: home");
    }

    public function likePost($postId, $location, $like = "")
    {
        $user = R::load('user', $_SESSION['loggedIn']);
        $post = R::load('feed', $postId);
    
        if ($like == 'like') {
            $post->likes++;
            $user->liked_posts .= $postId . ",";
        } else {
            $post->likes--;    
            $user->liked_posts = str_replace($postId . ",", "", $user->liked_posts);
        }
        R::store($post);
        R::store($user);
        header("location: " . $location);
        unset($_GET['likePostId'], $_GET['like']);     
    }

    public function returnPersonalPosts($userId)
    {
        $_SESSION['viewedUser'] = R::load('user', $userId);
     
        switch ($_GET['viewType']) {
            case 'fav':
                $_SESSION['viewType'] = 'fav';
                break;
            case 'all':
                $_SESSION['viewType'] = 'all';
                break;
            default:
        }

        switch ($_SESSION['viewType']) {
            case 'fav':
                $allBeans = $this->returnOthersLiked($userId);
                break;
            case 'all':
                $allBeans = R::find('feed', 'user_id = ?', [$userId]);  
                break;
            default:
        }

        return $allBeans;
    }

    public function returnOthersLiked($userId)
    {
        $user = R::load('user', $userId);
        $likedArrKeys = explode(',', $user->liked_posts);
        $likedArr = [];
    
        $posts = R::batch('feed', $likedArrKeys);
    
        foreach ($posts as $post) {
            array_push($likedArr, $post);
        }

        array_pop($likedArr);
    
        return $likedArr;
    }

    public function returnAllLikedIds()
    {
        $user = R::load('user', $_SESSION['loggedIn']);
        return explode(',', $user->liked_posts);
    }
}