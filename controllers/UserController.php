<?php

use RedBeanPHP\R;

class UserController extends BaseController
{
    public array $users = [];

    public function __construct()
    {
        $usersJsonArr = R::findAll('user');
        foreach ($usersJsonArr as $userJsonStr) {
            array_push($this->users, json_decode($userJsonStr, true, 512, 0));
        }
    }

    public function getUsernameById($id)
    {
        $user = R::findOne('user', 'id = ?', [$id]);
        return $user['username'];
    }

    public function handleAccounts($post)
    {
        if (isset($post['loginHidden'])) {
            $this->login($post);
            return;
        }
        if (isset($post['signupHidden'])) {
            $this->signup($post);
            return;
        }
    }

    private function login($post)
    {
        if ($post['userLogin'] == "" || $post['passLogin'] == "") {
            $_SESSION['currentError'] = 'All fields required';
            return;
        }

        $user = R::findOne('user', 'username = ?', [$post['userLogin']]);
        if ($user && password_verify($post['passLogin'], $user->password)) {
            $_SESSION['loggedIn'] = $user->id;
            header("location: ../feed/home");
            return;
        }

        $_SESSION['currentError'] = 'Username or password incorrect';
    }

    private function signup($post)
    {
        foreach ($post as $value) {
            if ($value == "") {
                $_SESSION['currentError'] = 'All fields required';
                return;
            }
        }
        $user = R::findOne('user', 'username = ?', [$post['userSignup']]);

        if (!$user) {
            if ($post['passSignup'] == $post['passSignupConfirm']) {
                $newUser = R::dispense('user');
                $newUser->username = $post['userSignup'];
                $newUser->password = password_hash($post['passSignup'], PASSWORD_DEFAULT);
                $newUser->profile_pic = "standard.jpg";
                $newUser->profile_pic_path = "../images/pfps/standard.jpg";
                R::store($newUser);
                $_SESSION['loggedIn'] = $newUser->id;
                header("location: ../feed/home");
                return;
            } else {
                $_SESSION['currentError'] = 'Passwords do not match';
                return;
            }
        } else {
            $_SESSION['currentError'] = 'Username already exists';
            return;
        }
    }

    public function logout()
    {
        session_destroy();
        header("location: ../user/login");
    }

    public function editUser($post)
    {
        $user = R::load('user', $_SESSION['loggedIn']);
        switch ($post['editType']) {
            case 'profilePic':
                if ($_FILES['fileUpload']['error'] == UPLOAD_ERR_OK) {
                    $allowed_mimes = array('image/jpeg', 'image/png');
                    $uploaded_mime = $_FILES['fileUpload']['type'];
                
                    if (in_array($uploaded_mime, $allowed_mimes)) {
                        $file = $_FILES['fileUpload']['tmp_name'];
                        $filename = $_FILES['fileUpload']['name'];
                
                        // Use GD to check if the uploaded file is an image
                        if (exif_imagetype($file)) {
                            $rename = uniqid("") . $filename;
                            $destination = '../images/pfps/' . $rename;
                            move_uploaded_file($file, $destination);
                            $user->profile_pic = $rename;
                            $user->profile_pic_path = $destination;
                        } else {
                            $_SESSION['editError'] = "Invalid image upload";
                        }
                    } else {
                        $_SESSION['editError'] = "Invalid image upload";
                    }
                }
                break;
            case 'username':
                if (strlen(trim($post['changeUser'])) > 0) {
                    $user->username = $post['changeUser'];
                } else {
                    $_SESSION['editError'] = "Invalid username";
                }
                break;
            case 'description':
                if (strlen(trim($post['changeDesc'])) > 0) {
                    $user->description = $post['changeDesc'];
                } else {
                    $_SESSION['editError'] = "Invalid description";
                }
                break;
            case 'password':
                if (strlen(trim($post['changePass'])) > 0) {
                    $user->password = password_hash($post['changePass'], PASSWORD_DEFAULT);
                } else {
                    $_SESSION['editError'] = "Invalid password";
                }
                break;
            default:
                header("location: inspect");
                die();
                break;
        }
        R::store($user);
    }
}
