<?php

    use Twig\Extension\DebugExtension;

    require_once 'vendor/autoload.php';

function displayTemplate($template, $array = [])
{
    $loader = new \Twig\Loader\FilesystemLoader('../views/');
    $twig = new \Twig\Environment($loader, [
    'debug' => true,
    'cache' => false
    ]);
    $twig->addExtension(new DebugExtension());
    $twig->addGlobal('session', $_SESSION);
    $twig->addGlobal('post', $_POST);
    $twig->addGlobal('get', $_GET);
        
    $template = $twig->display($template, $array);
}

function error($errorNumber, $errorMessage)
{
    $loader = new \Twig\Loader\FilesystemLoader('../views/');
    $twig = new \Twig\Environment($loader, [
    'debug' => true,
    'cache' => false
    ]);
    $twig->addExtension(new DebugExtension());

    $array = ['message' => $errorMessage, 'code' => $errorNumber];
        
    $template = $twig->display("error.twig", $array);

    http_response_code($errorNumber);
}
