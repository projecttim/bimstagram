<?php

require_once '../vendor/autoload.php';
use RedBeanPHP\R;

session_start();

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(1);

$base = new BaseController();
$user = new UserController();
$feed = new FeedController();
$comment = new CommentController();

$user->handleAccounts($_POST);

$q = $_GET['q'] ?? null;

$location = parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH);

if ($q == "") {
    header("location: user/login");
    die();
}

switch ($q) {
    case 'user/login':
        if (isset($_SESSION['loggedIn'])) {
            header("location: ../feed/home");
            die();
        }
        break;
    case 'user/register':
        break;
    case 'feed/home':
        $base->authorizeUser($q);
        $feed->clearSes();
        break;
    default:
        $base->authorizeUser($q);
        break;
}

if (isset($_GET['logout'])) {
    $user->logout();
}
if (isset($_POST['editType'])) {
    $user->editUser($_POST);
}
if (isset($_POST['commentCheck'])) {
    $comment->newComment($_POST, $location);
}
if (isset($_POST['searchQuery'])) {
    $feed->returnSearchResult($_POST['searchQuery']);
}
if (isset($_GET['likePostId'])) {
    $feed->likePost($_GET['likePostId'], $location, $_GET['like']);
}
if (isset($_POST['create'])) {
    $feed->createPost($_POST);
}
if (isset($_GET['showId'])) {
    $personalPosts = $feed->userFeedView($_GET['showId']);
}
if (isset($_SESSION['showId'])) {
    $personalPosts = $feed->userFeedView($_SESSION['showId']);
}

if (file_exists("../views/" . $q . ".twig")) {
    displayTemplate($q . ".twig", [
        'posts' => $base->showAll('feed'),
        'userBean' => $base->getBeanById('user', $_SESSION['loggedIn']),
        'allComments' => $base->showAll('comment'),
        'allLiked' => $feed->returnAllLikedIds(),
        'location' => $location,
        'fullUrl' => $base->getFullUrl(),
        'personPosts' => $personalPosts,
    ]);
    unset($_SESSION['currentError']);
    unset($_SESSION['editError']);
} else {
    error(404, "File not found");
}
