<?php

include 'vendor/autoload.php';
use RedBeanPHP\R;

$users = [
    [
        'username' => 'Bim',
        'password' => 'Bim'
    ],
    [
        'username' => 'admin',
        'password' => 'admin'
    ],
    [
        'username' => 'Pietje',
        'password' => 'Pietje'
    ],
    [
        'username' => 'Henk',
        'password' => 'Henk'
    ]
];

$posts = [
    [
        'username' => 'admin',
        'title' => 'wow zo leuk en cool',
        'snippet' => '<input name="searchQuery" type="text" placeholder="Search by username" class="outline-none w-96">',
        'language' => 'html',
        'likes' => 1,
        'comments' => [
            [
                'username' => 'Bim',
                'comment' => 'wow deze code is erg tof jo'
            ],
            [
                'username' => 'admin',
                'comment' => 'ja ik comment onder mn eigen post'
            ]
        ]
    ],
    [
        'username' => 'Bim',
        'title' => 'ik ben er erg trots op',
        'snippet' => 'return $this->returnPersonalPosts($userId);',
        'language' => 'php',
        'likes' => 44,
        'comments' => [
            [
                'username' => 'admin',
                'comment' => 'jaa ik ben admin'
            ],
            [
                'username' => 'Henk',
                'comment' => 'ik vind dit geen leuke code'
            ]
        ]
    ],
    [
        'username' => 'admin',
        'title' => 'supa toff',
        'snippet' => 'type variable_list = value_list;',
        'language' => 'Lua',
        'likes' => 4,
        'comments' => [
            [
                'username' => 'Bim',
                'comment' => 'ja dit is wel echt tof hee'
            ],
            [
                'username' => 'Pietje',
                'comment' => 'pietj'
            ]
        ]
    ],
    [
        'username' => 'Pietje',
        'title' => 'jaa ik ben pietje',
        'snippet' => 'cout << "Hello World!";',
        'language' => 'c++',
        'likes' => 69,
        'comments' => [
            [
                'username' => 'Henk',
                'comment' => 'tof ouwe, leuke post'
            ],
            [
                'username' => 'Pietje',
                'comment' => 'helemaal klote'
            ]
        ]
    ]
];

//Check of de database naam overeenkomt met je eigen database in PHPMyAdmin
//                                         \/\/\/\/\/\/\/\/\/

try {
    R::setup('mysql:host=localhost;dbname=binsta', 'bit_academy', 'bit_academy');
} catch (PDOException $e) {
    echo $e->getmessage();
}  

R::nuke();

foreach ($users as $user) {
    $pfp = 'standard.jpg';
    $newUser = R::dispense('user');
    $newUser->username = $user['username'];
    $newUser->password = password_hash($user['password'], PASSWORD_DEFAULT);
    $newUser->profile_pic = $pfp;
    $newUser->profile_pic_path = "../images/pfps/" . $pfp;
    foreach ($posts as $post) {
        if ($post['username'] == $user['username']) {
            $newPost = R::dispense('feed');
            $newPost->title = $post['title'];
            $newPost->username = $post['username'];
            $newPost->snippet = $post['snippet'];
            $newPost->language = $post['language'];
            $newPost->likes = $post['likes'];
            $newUser->ownPostList[] = $newPost;
            foreach ($post['comments'] as $comment) {
                $newComment = R::dispense('comment');
                $newComment->commentContent = $comment['comment'];
                $newPost->ownCommentList[] = $newComment;
            }
        }
    }
    R::store($newUser);
}

foreach ($posts as $post) {
    foreach ($post['comments'] as $comment) {
        $user = R::findOne('user', 'username = ?', [$comment['username']]);
        $commentDB = R::findOne('comment', 'comment_content = ?', [$comment['comment']]);
        $user->ownCommentList[] = $commentDB;
        R::store($user);
    }
}



echo count($users) . " users toegevoegd!" . PHP_EOL;
echo count($posts) . " posts toegevoegd!" . PHP_EOL;
echo count(R::findAll('comment')) . " comments toegevoegd!";