/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./views/**/*.{html,js,twig}",
  ],
  theme: {
    fontFamily: {
      'italiana': ['italiana'],
      'SourceCodePro': ['Source Code Pro']
    },
    extend: {},
  },
  plugins: [],
}
